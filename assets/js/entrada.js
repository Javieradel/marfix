'use strict'
const nav = document.querySelector('#header-nav-btn');
const navbar = document.querySelector(' header nav.header-nav');
/**
 *
 */
const section = document.querySelectorAll('article.section-content');

function manejoSideBar(e) {

    if (navbar.classList.contains('header-nav')) {
        navbar.classList.remove('header-nav');
        navbar.classList.add('toolbar');
        nav.classList.add('inv');
    } else {
        navbar.classList.remove('toolbar');
        navbar.classList.add('header-nav');
        nav.classList.remove('inv');
    }
}
nav.addEventListener('click', manejoSideBar, false);

/**
 *
 *
 */
function collapse(ind) {
    //article main p
    section[ind].children[1].classList.remove('visible-content');
    section[ind].children[1].classList.remove('fadeInDown');




    section[ind].children[1].classList.add('fadeOutUpBig');

    setTimeout(() => {
        section[ind].children[1].classList.add('hidden-content');
    }, 400)
}

function expand(ind) {
    //article main p
    section[ind].children[1].classList.remove('hidden-content');
    section[ind].children[1].classList.remove('fadeOutUpBig');
    section[ind].children[1].classList.add('visible-content');


    section[ind].children[1].classList.add('fadeInDown');

}
section.forEach((elem, indx) => {

    //article header buttom
    //section[1].children[0].lastElementChild
    elem.children[0].lastElementChild.addEventListener('click', (e) => {
        if (elem.children[1].classList.contains('hidden-content')) {
            console.log('expand');
            expand(indx);
        } else {
            collapse(indx);
            console.log('collapse');
        }

    })
});
/**
 *
 *
 */