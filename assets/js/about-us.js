const nav = document.querySelector('#header-nav-btn');
const navbar = document.querySelector(' header nav.header-nav');
const IMAGENES = document.querySelectorAll('#targetas article.carousel ');
const $imagen = document.querySelector('#targetas');
new WOW().init();

function manejoSideBar(e) {

    if (navbar.classList.contains('header-nav')) {
        navbar.classList.remove('header-nav');
        navbar.classList.add('toolbar');
        nav.classList.add('inv');
    } else {
        navbar.classList.remove('toolbar');
        navbar.classList.add('header-nav');
        nav.classList.remove('inv');
    }
}

nav.addEventListener('click', manejoSideBar, false);

/*********************
 * *******************
 * ************* */
//slider
const imgs = document.querySelectorAll('#sliderWrapp .item');
const clsEntrada = 'fadeInRight',
    clsSalida = 'fadeOutLeft';
let position = 0;

function avanzar() {
    console.log(position);
    /****
     *Retorno
     * ** */
    if (position >= imgs.length - 1) {
        position = 0;

        imgs[imgs.length - 1].style.display = "none";

        imgs[position].classList.add(clsEntrada);
    } else {

        /**
         * ocultar
         */
        imgs[position].style.display = "none";
        /**
         * Incrementar
         */
        position++;
        /**
         * Mostrar
         */
        imgs[position].classList.add(clsEntrada);


    }
    imgs[position].style.display = "block";
}
setInterval(avanzar, 6000);