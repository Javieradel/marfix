const nav = document.querySelector('#header-nav-btn');
const navbar = document.querySelector(' header nav.header-nav');
const linksWsMulticonexion = [
    'https://wa.me/584245273604?text=Interés%20en%20el%20plan%20Multiconexión%206Mb',
    'https://wa.me/584245273604?text=Interés%20en%20el%20plan%20Multiconexión%207Mb',
    'https://wa.me/584245273604?text=Interés%20en%20el%20plan%20Multiconexión%2010Mb',
    'https://wa.me/584245273604?text=Interés%20en%20el%20plan%20Multiconexión%2015Mb',
    'https://wa.me/584245273604?text=Interés%20en%20el%20plan%20Multiconexión%2020Mb',
    'https://wa.me/584245273604?text=Interés%20en%20el%20plan%20Multiconexión%2030Mb',
    ''
];
const linksWsMulticonexionForaneo = [
    'https://wa.me/584245273604?text=Interés%20en%20el%20plan%20Multiconexión%20Foráneo%204Mb',
    'https://wa.me/584245273604?text=Interés%20en%20el%20plan%20Multiconexión%20Foráneo%205Mb',
    'https://wa.me/584245273604?text=Interés%20en%20el%20plan%20Multiconexión%20Foráneo%206Mb',
    'https://wa.me/584245273604?text=Interés%20en%20el%20plan%20Multiconexión%20Foráneo%2010Mb',
    'https://wa.me/584245273604?text=Interés%20en%20el%20plan%20Multiconexión%20Foráneo%2020Mb',
    ''
];
const linksWsDedicado = [
    'https://wa.me/584245273604?text=Interés%20en%20el%20plan%20dedicado%205Mb',
    'https://wa.me/584245273604?text=Interés%20en%20el%20plan%20dedicado%206Mb',
    'https://wa.me/584245273604?text=Interés%20en%20el%20plan%20dedicado%2010Mb',
    'https://wa.me/584245273604?text=Interés%20en%20el%20plan%20dedicado%2020Mb',
    'https://wa.me/584245273604?text=Interés%20en%20el%20plan%20dedicado%2025Mb',
    'https://wa.me/584245273604?text=Interés%20en%20el%20plan%20dedicado%2050Mb',
    'https://wa.me/584245273604?text=Interés%20en%20el%20plan%20dedicado%2060Mb'
]
const linksWsGamer = [
    'https://wa.me/584245273604?text=Interés%20en%20el%20plan%20dedicado%20%20Gamer%206Mb',
    'https://wa.me/584245273604?text=Interés%20en%20el%20plan%20dedicado%20Gamer%206Mb',
    'https://wa.me/584245273604?text=Interés%20en%20el%20plan%20dedicado%20%20Gamer%2010Mb',
    'https://wa.me/584245273604?text=Interés%20en%20el%20plan%20dedicado%20%20Gamer%2020Mb',
    'https://wa.me/584245273604?text=Interés%20en%20el%20plan%20dedicado%20%20Gamer%2025Mb',
    'https://wa.me/584245273604?text=Interés%20en%20el%20plan%20dedicado%20%20Gamer%2050Mb',
    'https://wa.me/584245273604?text=Interés%20en%20el%20plan%20dedicado%20%20Gamer%2060Mb',

]
const linksWsIP = [
    'https://wa.me/584245273604?text=Interés%20en%20el%20plan%20dedicado%20con%20IP%20pública%205Mb',
    'https://wa.me/584245273604?text=Interés%20en%20el%20plan%20dedicado%20con%20IP%20pública%2010Mb',
    'https://wa.me/584245273604?text=Interés%20en%20el%20plan%20dedicado%20con%20IP%20pública%2020Mb',
    'https://wa.me/584245273604?text=Interés%20en%20el%20plan%20dedicado%20con%20IP%20pública%2030Mb',
    'https://wa.me/584245273604?text=Interés%20en%20el%20plan%20dedicado%20con%20IP%20pública%2040Mb',
    'https://wa.me/584245273604?text=Interés%20en%20el%20plan%20dedicado%20con%20IP%20pública%2050Mb'
]
const planes = ['plans-multiconexion.html', 'plans-dedicado.html', 'plans-gamer.html', 'plans-ip-publica.html'];
new WOW().init();

function manejoSideBar(e) {

    if (navbar.classList.contains('header-nav')) {
        navbar.classList.remove('header-nav');
        navbar.classList.add('toolbar');
        nav.classList.add('inv');
    } else {
        navbar.classList.remove('toolbar');
        navbar.classList.add('header-nav');
        nav.classList.remove('inv');
    }
}

nav.addEventListener('click', manejoSideBar, false);

/*

**************
*/

function enviarWs(obj, arrSms) {

    obj.forEach((elem, indx) => {
        console.log(elem, arrSms[indx]);

        elem.addEventListener('click', (e) => {
            window.location = arrSms[indx];
        })
    })
}


if (document.title == 'Marfix | Conoce nuestros planes.') {
    const btn = document.querySelector('.btn');
    btn.addEventListener('click', () => {
        window.location = "https://wa.me/584245273604?text=Tengo%20interés%20por%20saber%20más%20de%20sus%20planes%20y%20servicios";
    })
    const targetPriceNode = document.querySelectorAll('.feature');
    enviarWs(targetPriceNode, planes);
}
if (document.title == 'Marfix | Internet Banda Ancha para tu casa o negocio.') {
    const targetPriceNode = document.querySelectorAll('.micro .feature');
    enviarWs(targetPriceNode, linksWsMulticonexion);
    const targetPriceForain = document.querySelectorAll('.foranea .feature');
    enviarWs(targetPriceForain, linksWsMulticonexionForaneo);

    const btn = document.querySelector('.btn');
    btn.addEventListener('click', () => {
        window.location = "https://wa.me/584245273604?text=Interés%20en%20saber%20más%20sobre%20los%20planes%20Multiconexión";
    })


}
if (document.title == 'Marfix | Internet Banda Ancha para tu negocio.') {
    const targetPrice = document.querySelectorAll('.feature');
    enviarWs(targetPrice, linksWsDedicado);

    const btn = document.querySelector('.btn');
    btn.addEventListener('click', () => {
        window.location = "https://wa.me/584245273604?text=Interés%20en%20saber%20más%20sobre%20los%20planes%20dedicados";
    })


}
if (document.title == 'Marfix | Juega sin Lag') {
    const targetPrice = document.querySelectorAll('.feature');
    enviarWs(targetPrice, linksWsGamer);

    const btn = document.querySelector('.btn');
    btn.addEventListener('click', () => {
        window.location = "https://wa.me/584245273604?text=Interés%20en%20saber%20más%20sobre%20los%20planes%20dedicados%20Gamer";
    })


}
if (document.title == 'Marfix | Internet Banda Ancha negocio o empresa.') {
    const targetPrice = document.querySelectorAll('.feature');
    enviarWs(targetPrice, linksWsIP);

    const btn = document.querySelector('.btn');
    btn.addEventListener('click', () => {
        window.location = "https://wa.me/584245273604?text=Interés%20en%20saber%20más%20sobre%20los%20planes%20dedicados%20con%20IP%20pública";
    })


}