<?php
//var_dump($_SERVER);
//echo $_SERVER['REQUEST_METHOD'];
//header("content-type: application/json");
require_once('./conf.php');
require_once('./functions.php');
$err=array();
$metodo = $_SERVER['REQUEST_METHOD']; 

//$mysqli = new mysqli("localhost","admin","admin","db_usuarios");

switch($metodo){
    case 'GET':
        try{
                $statament = $DB->prepare('SELECT * FROM `muestra-personas`');
                $statament-> execute();
                $busqueda = $statament->fetchAll(PDO::FETCH_ASSOC);
                echo json_encode($busqueda);
        }catch(PDOException $e){
               // si ocurre un error hacemos rollback para anular todos los insert
                echo "Error:".$e->getMessage();
            }

        
        break;
    case 'POST':
        
        $data = json_decode(file_get_contents("php://input"),true);
        //echo json_encode($data);
        //echo "<pre>".print_r ($_POST)."</pre></br>" ;
        echo "<pre>".print_r ($data)."</pre></br>" ;
        
        if(empty($_POST)){

            if(stringEmpty($data['name'])){
                $name= trim(filter_var(htmlspecialchars($data['name']),FILTER_SANITIZE_STRING));
            }else{
                echo json_encode($err['err']= 'Nombre no puede estar vacio');
                exit();

            }
            if(stringEmpty($data['email'])){
                $email= trim(filter_var(htmlspecialchars($data['email']),FILTER_SANITIZE_EMAIL));
            }else{
                echo json_encode($err['err']= 'Email no puede estar vacio');
                exit();
            }
            
            if(stringEmpty($data['isp'])){
                $isp=sanitize($data['isp'],'string');
            }
            if(stringEmpty($data['comentario'])){
                $comentario=trim(filter_var(htmlspecialchars($data['comentario']),FILTER_SANITIZE_STRING));
            }
            if(stringEmpty($data['contact']['telf'][0])){
                $dataContact['contact']['telf'][0]=trim(filter_var(htmlspecialchars($data['contact']['telf'][0]),FILTER_SANITIZE_STRING));
            }else{
                echo json_encode($err['err']= 'Debe haber por lo menos un número');
                exit();
            }
            if(stringEmpty($data['contact']['whatsapp'])){
                $dataContact['contact']['whatsapp']=trim(filter_var(htmlspecialchars($data['contact']['whatsapp']),FILTER_SANITIZE_STRING));
            }
            if(stringEmpty($data['contact']['telegram'])){
                $dataContact['contact']['telegram']=trim(filter_var(htmlspecialchars($data['contact']['telegram']),FILTER_SANITIZE_STRING));
            }
            if(stringEmpty($data['contact']['instagram'])){
                $dataContact['contact']['instagram']=trim(filter_var(htmlspecialchars($data['contact']['instagram']),FILTER_SANITIZE_STRING));
            }
            if( isset($data['ubicacion']['estado']) ){
              
                $dataUbicate['ubicacion']['sector']=trim(filter_var(htmlspecialchars($data['ubicacion']['sector']),FILTER_SANITIZE_STRING));
                $dataUbicate['ubicacion']['municipio']=trim(filter_var(htmlspecialchars($data['ubicacion']['municipio']),FILTER_SANITIZE_STRING));
                $dataUbicate['ubicacion']['estado']=trim(filter_var(htmlspecialchars($data['ubicacion']['estado']),FILTER_SANITIZE_STRING));
                if($data['ubicacion']['gcord']!= 'N/A'){
                    $dataUbicate['ubicacion']['gcord']=trim(htmlspecialchars($data['ubicacion']['gcord']));
                }else{
                    $dataUbicate['ubicacion']['gcord']=null;
                }
            }else{
                echo json_encode($err['err']= 'Debe haber por lo menos una dirección');
                exit();
            }

            //determinar si existe ya un correo similar
            //$statament = $DB->prepare('SELECT `name` FROM `muestra-personas` WHERE :email = `muestra-personas`.`email` OR (:sector = `direccion`.`sector` AND :municipio = `direccion`.`municipio` AND :estado = `direccion`.`estado`)');
        try{

            $statament = $DB->prepare('SELECT `name` FROM `muestra-personas` WHERE :email = `muestra-personas`.`email`');
            $statament-> execute(array(':email'=>$email));
            $busqueda = $statament->fetch();
            $statament = null;
            if($busqueda == false){
                //registrar
                $statament = $DB->prepare('INSERT INTO `muestra-personas` (`id`,`name`,`email`,`wb-active`) VALUES (null,:name,:email,:isp)');
                $statament-> execute(array(':email'=>$email,':name'=>$name,':isp'=>$isp));
                $lastId = $DB->lastInsertId();

                if($lastId != false){
                    //direccion
                    $statament = $DB->prepare('INSERT INTO `direccion` (`id`,`id-person`,`sector`,`municipio`,`estado`,`gcords`) VALUES (null,:lastId,:sector,:municipio,:estado,:gcords)');
                    $statament-> execute(array(':sector'=>$dataUbicate['ubicacion']['sector'],':municipio'=>$dataUbicate['ubicacion']['municipio'],':estado'=>$dataUbicate['ubicacion']['estado'],':gcords'=>$dataUbicate['ubicacion']['gcord'],':lastId'=>$lastId));
                    //contacto
                    $statament = $DB->prepare('INSERT INTO `redes` (`id`,`id-person`,`instagram`,`whatsapp`,`telegram`) VALUES (null,:lastId,:instagram,:whatsapp,:telegram)');
                    $statament-> execute(array(':whatsapp'=>$dataContact['contact']['whatsapp'],':instagram'=>$dataContact['contact']['instagram'],':telegram'=>$dataContact['contact']['telegram'],':lastId'=>$lastId));
                    //telf
                    for($i=0;$i<count($dataContact['contact']['telf']);$i++){
                        echo $dataContact['contact']['telf'][$i];
                        $statament = $DB->prepare('INSERT INTO `telf-personas` (`id`,`id-person`,`telf`) VALUES (null,:lastId,:telf)');
                        $statament-> execute(array(':telf'=>$dataContact['contact']['telf'][$i],':lastId'=>$lastId));
                    }   
                    
                }
            }else{
                echo json_encode($err['err']= 'Email ya en uso');
                exit();
            }
        }catch(PDOException $e){
               // si ocurre un error hacemos rollback para anular todos los insert
                echo "Error:".$e->getMessage();
                $DB->rollback();
            }
        
        /*    //
        $statament = $DB->prepare('SELECT * FROM `muestra-personas` WHERE `muestra-personas`.`id` = :id ');
        $statament-> execute(array(':id'=>$lastId));
        $res = $statament->fetch(PDO::FETCH_ASSOC);
        print_r($res); */
        



        }
        break;
    case 'PUT':
        echo "nothing to PUT";
        break;
}  
?>
 