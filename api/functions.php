<?php
define('vName','/^[A-Za-z]+\s[A-Za-z]+$/');
define('vTelf','/^\d{6,15}$/');
define('vEmail','/^[\w-\.]+@([\w-]+\.)+[\w-]{2,4}$/');

/* 
function rjc_tofloat($num) {
    $num=trim($num);
    if ($num==='') {return 0;}
    else {
         do {
             $signo=$num[0]; $num=substr($num, 1);
             $Ok=strpos("-+.0123456789",$signo);
         }
         while($num!=='' && $Ok===false);
         if ($signo==='-' || $signo==='+') {$resulta=$num;}
         else if ($signo==='.') {$resulta='0.'.$num; $signo='';}
         else {$resulta=$signo.$num; $signo='';}
         if ($resulta==='') {return 0;}
        $num=$resulta;
        $dotPos = strrpos($num, '.');
        $commaPos = strrpos($num, ',');
        $sep = (($dotPos > $commaPos) && $dotPos) ? $dotPos :
                   ((($commaPos > $dotPos) && $commaPos) ? $commaPos : false);
        if (!$sep) {return floatval($signo.preg_replace("/[^0-9]/", "", $num));}
        return floatval($signo.
                   preg_replace("/[^0-9]/", "", substr($num, 0, $sep)) . '.' .
                   preg_replace("/[^0-9]/", "", substr($num, $sep+1, strlen($num)))
        );
    }
} */
function stringEmpty($st){
    if(strlen($st) == 0 || $st == ' '){
        return 0;
    }else{
        return $st;
    }
    
}
function sanitize($data, $filter){
    switch ($filter) {
        case 'string':
            if(is_string($data)){
               return $data=trim(filter_var(htmlspecialchars($data),FILTER_SANITIZE_STRING));

            }else{
                return 0;
            }
            
            break;
        case 'number':
            if(is_numeric($data)){
                if(is_int($data)){
                    return $data=trim(filter_var(htmlspecialchars($data),FILTER_SANITIZE_NUMBER_INT));
                }elseif(is_float($data)){
                    return $data=trim(filter_var(htmlspecialchars($data),FILTER_SANITIZE_NUMBER_FLOAT));
                }else{
                    return 0;
                }
            }else{
                return 0;
            }
            
        break;
        case 'email':
            $data=trim(filter_var(htmlspecialchars($data),FILTER_SANITIZE_EMAIL));
            echo $data;
            if(preg_match(vEmail,$data)!=0){
                return $data ;
            }else{
                return 0;
            }
            break;
        case 'name':
            $data=trim(filter_var(htmlspecialchars($data),FILTER_SANITIZE_STRING));
            echo $data;
            if(preg_match(vName,$data)!=0){
                return $data ;
            }else{
                return false;
            }
            break;
        case 'telf':
            $data=trim(filter_var(htmlspecialchars($data),FILTER_SANITIZE_NUMEBER_INT));
            if(preg_match(vTelf,$data)!=0){
                return $data ;
            }else{
                return false;
            }
            break;
                    
        default:
            return 0;
            break;
    }
}
?>